#' @importFrom magrittr %>%
NULL
#' @importFrom rlang .data
NULL
#' Traer datos de rendimiento para un fondo
#'
#' Función auxiliar para `rendimiento_fondo()` para pedir datos de
#' rendimiento históricos para un fondo común de inversión.
#'
#' @param id_fondo Identificador del fondo
#' @param id_clase Identificador de clase del fondo
#' @param desde,hasta Fechas
#' @param movimientos Booleano indicando si los datos devueltos deberían incluir
#' fechas intermedias
#' @return Un data frame
rendimiento_fondo_retriever <- function(id_fondo,
                                        id_clase,
                                        desde,
                                        hasta,
                                        movimientos) {
  # Define helper functions for calculating variation for specific periods
  rendimiento_period_calculator <- function(df, period_from, period_to) {
    # Create period label
    year_from <- base::as.character(
      lubridate::year(df$desde[df$periodo == period_from]) + 1
    )
    year_to <- base::as.character(
      lubridate::year(df$desde[df$periodo == period_to])
    )
    period_label <- base::paste(
      base::unique(base::c(year_from, year_to)),
      collapse = " a "
    )

    tibble::tibble(periodo = !!period_label,
                   desde = df$desde[df$periodo == !!period_from],
                   hasta = df$desde[df$periodo == !!period_to],
                   valor_inicio = df$valor_inicio[df$periodo == !!period_from],
                   valor_fin = df$valor_inicio[df$periodo == !!period_to],
                   variacion = (.data$valor_fin / .data$valor_inicio - 1) * 100)
  }

  # Set base URL for CAFCI API
  url_base <- "https://api.cafci.org.ar"

  # If no dates are defined, get general yield data from `ficha` endpoint
  if (base::all(base::is.null(desde), base::is.null(hasta))) {
    url <- base::paste0(
      url_base, "/fondo/", id_fondo, "/clase/", id_clase, "/ficha"
    )

    response <- try_get(url)
    if (!base::is.null(response$data$info$diaria$rendimientos)) {
      # Create base dataframe
      df <- dplyr::bind_rows(
        response$data$info$diaria$rendimientos, .id = "periodo"
      ) %>%
        dplyr::filter(.data$periodo != "monthYear") %>%
        dplyr::rename(
          "desde" = .data$fecha,
          "variacion" = .data$rendimiento
        ) %>%
        dplyr::mutate(
          desde = base::as.Date(
            base::format(
              base::as.POSIXct(.data$desde, format = "%d/%m/%Y"),
              "%Y-%m-%d"
            )
          ),
          variacion = base::as.numeric(.data$variacion)
        ) %>%
        dplyr::mutate(
          hasta = base::as.Date(
            base::format(
              base::as.POSIXct(
                response$data$info$diaria$referenceDay,
                format = "%d/%m/%Y"
              ),
              "%Y-%m-%d"
            )
          ),
          .after = desde
        ) %>%
        dplyr::mutate(
          valor_fin = base::as.numeric(response$data$info$diaria$actual$vcp),
          .after = .data$hasta
        ) %>%
        dplyr::mutate(
          valor_inicio = .data$valor_fin / (1 + (.data$variacion / 100)),
          .before = .data$valor_fin
        ) %>%
        # Drop `variacion` and `tna` columns (will be calculated later)
        dplyr::select(-.data$variacion, -.data$tna) %>%
        # Manually arrange periods
        dplyr::arrange(
          base::match(
            .data$periodo,
            c("day", "month", "oneYear", "threeYears", "fiveYears",
              "year", "yearM1", "yearM2", "yearM3", "yearM4")
          )
        )

      # Calculate custom periods
      df <- df %>%
        dplyr::bind_rows(
          # Y - 1 (last year)
          rendimiento_period_calculator(df, "yearM1", "year"),
          # (Y - 2) to (Y - 1)
          rendimiento_period_calculator(df, "yearM2", "year"),
          # (Y - 3) to (Y - 1)
          rendimiento_period_calculator(df, "yearM3", "year"),
          # (Y - 4) to (Y - 1)
          rendimiento_period_calculator(df, "yearM4", "year"),
          # Y - 2 (previous to last year)
          rendimiento_period_calculator(df, "yearM2", "yearM1"),
          # (Y - 3) to (Y - 2)
          rendimiento_period_calculator(df, "yearM3", "yearM1"),
          # (Y - 4) to (Y - 2)
          rendimiento_period_calculator(df, "yearM4", "yearM1"),
          # Y - 3
          rendimiento_period_calculator(df, "yearM3", "yearM2"),
          # (Y - 4) to (Y - 3)
          rendimiento_period_calculator(df, "yearM4", "yearM2"),
          # Y - 4
          rendimiento_period_calculator(df, "yearM4", "yearM3")
        ) %>%
        # Calculate yield stats for periods
        dplyr::mutate(
          variacion = (.data$valor_fin / .data$valor_inicio - 1) * 100
        ) %>%
        dplyr::mutate(tna = calculate_tna(.data$desde,
                                          .data$hasta,
                                          .data$variacion)) %>%
        dplyr::mutate(tca = calculate_tca(.data$desde,
                                          .data$hasta,
                                          .data$variacion)) %>%
        # Rename periods
        dplyr::rowwise() %>%
        dplyr::mutate(
          periodo = dplyr::case_when(
            .data$periodo == "day" ~ "\u00FAltimo d\u00EDa",
            .data$periodo == "month" ~ "mes actual",
            .data$periodo == "oneYear" ~ "1 a\u00F1o",
            .data$periodo == "threeYears" ~ "3 a\u00F1os",
            .data$periodo == "fiveYears" ~ "5 a\u00F1os",
            base::grepl("^year(M[0-9])*$", .data$periodo) ~ base::paste(
              base::unique(
                base::c(
                  lubridate::year(.data$desde) + 1,
                  lubridate::year(.data$hasta)
                )
              ),
              collapse = " a "
            ),
            .default = .data$periodo
          )
        ) %>%
        dplyr::ungroup() %>%
        dplyr::filter(!base::is.na(.data$periodo)) %>%
        # Remove periods with no data
        dplyr::filter(!base::is.na(.data$variacion))

      # Arrange periods
      df <- dplyr::arrange(df, .data$periodo)
      df[arrange_periods(df$periodo), ]
    } else {
      empty_df()
    }
  } else {
    # Otherwise, if dates are defined, use `rendimiento` endpoint
    if (movimientos == FALSE) step <- 0 else step <- 1

    url <- base::paste0(
      url_base, "/fondo/", id_fondo, "/clase/", id_clase,
      "/rendimiento/", desde, "/", hasta, "?step=", step
    )
    response <- try_get(url)

    if (step == 0) {
      if (!base::is.null(response$data)) {
        tibble::tibble(
          periodo = paste(
            base::as.Date(base::format(
              base::as.POSIXct(response$data$desde$fecha, format = "%d/%m/%Y"),
              "%Y-%m-%d"
            )),
            "a",
            base::as.Date(base::format(
              base::as.POSIXct(response$data$hasta$fecha, format = "%d/%m/%Y"),
              "%Y-%m-%d"
            ))
          ),
          desde = base::as.Date(base::format(
            base::as.POSIXct(response$data$desde$fecha, format = "%d/%m/%Y"),
            "%Y-%m-%d"
          )),
          hasta = base::as.Date(base::format(
            base::as.POSIXct(response$data$hasta$fecha, format = "%d/%m/%Y"),
            "%Y-%m-%d"
          )),
          valor_inicio = base::as.numeric(response$data$desde$valor),
          valor_fin = base::as.numeric(response$data$hasta$valor)
        ) %>%
          dplyr::mutate(
            variacion = (.data$valor_fin / .data$valor_inicio - 1) * 100
          ) %>%
          dplyr::mutate(tna = calculate_tna(.data$desde,
                                            .data$hasta,
                                            .data$variacion)) %>%
          dplyr::mutate(tca = calculate_tca(.data$desde,
                                            .data$hasta,
                                            .data$variacion))
      } else {
        empty_df()
      }
    } else {
      if (!base::is.null(response$data)) {
        base::rbind(
          tibble::as_tibble(response$data$desde)[1, ],
          tibble::as_tibble(response$data$hasta)
        ) %>%
          dplyr::mutate(
            fecha = base::as.Date(base::format(
              base::as.POSIXct(.data$fecha, format = "%d/%m/%Y"),
              "%Y-%m-%d"
            )),
            valor = base::as.double(.data$valor)
          ) %>%
          # If `desde` == `hasta`, then only keep last day
          dplyr::filter(
            dplyr::case_when(
              !!desde == !!hasta ~ .data$fecha == max(.data$fecha),
              .default = TRUE
            )
          ) %>%
          # Calculate variation between dates
          dplyr::mutate(
            variacion = (c(
              NA_real_,
              .data$valor[-1] / .data$valor[-base::length(.data$valor)]
            ) - 1) * 100
          ) %>%
          dplyr::arrange(.data$fecha) %>%
          # Calculate normalized `valor`
          dplyr::mutate(
            valor_rel = c(
              1,
              base::cumprod((100 + .data$variacion[-1]) / 100)
            ),
            .after = .data$valor
          )
      } else {
        tibble::tibble(fecha = lubridate::Date(),
                       valor = base::numeric(),
                       variacion = base::numeric())
      }
    }
  }
}
