Obtener y graficar datos de fondos comunes de inversión en la Argentina
================
Gas Panic!
2024-02-04 00:09:16.174924

- [1 Información general](#1-información-general)
- [2 Instalación](#2-instalación)
- [3 Lista de funciones](#3-lista-de-funciones)
- [4 Ejemplos de uso](#4-ejemplos-de-uso)
  - [4.1 Buscar fondos](#41-buscar-fondos)
  - [4.2 Datos de rendimiento](#42-datos-de-rendimiento)
    - [4.2.1 Períodos preestablecidos](#421-períodos-preestablecidos)
    - [4.2.2 Períodos definidos](#422-períodos-definidos)
    - [4.2.3 Movimientos](#423-movimientos)
  - [4.3 Graficar rendimientos](#43-graficar-rendimientos)
    - [4.3.1 Rendimientos totales](#431-rendimientos-totales)
    - [4.3.2 Evolución de rendimientos
      (movimientos)](#432-evolución-de-rendimientos-movimientos)
  - [4.4 Otras métricas: dólar, inflación y
    MERVAL](#44-otras-métricas-dólar-inflación-y-merval)
  - [4.5 Comparar categorías de
    fondos](#45-comparar-categorías-de-fondos)
- [5 Lista de endpoints conocidos de la API de
  CAFCI](#5-lista-de-endpoints-conocidos-de-la-api-de-cafci)

# 1 Información general

El objetivo de `fci` es darle al usuario algunas herramientas para
obtener datos actuales e históricos de fondos comunes de inversión (FCI)
en la Argentina, y así poder evaluar si los FCI representan una buena
alternativa de inversión.

Para traer datos de fondos, el paquete usa una API “escondida”
(<https://api.cafci.org.ar>) de la *Cámara Argentina de Fondos Comunes
de Inversión* (CAFCI). Como no existe ninguna documentación *pública* de
la API, las funciones del paquete usan algunos de los *endpoints* que
logré identificar analizando el tráfico de solicitudes en la página web
de CAFCI. Hay una lista de los endpoints disponibles [al final del
Readme](#5-lista-de-endpoints-conocidos-de-la-api-de-cafci); si conocés
más ejemplos, o parámetros para modificar las solicitudes, avisá!

Ningún análisis de inversiones en la Argentina está completo sin tomar
en consideración el cambio del dólar y/o la inflación. Por ende, el
paquete tiene también funciones para traer datos del dólar (traídos de
[Ámbito](https://mercados.ambito.com/)), así como la inflación y el
índice MERVAL (traídos de [Estadísticas
BCRA](https://api.estadisticasbcra.com)).

# 2 Instalación

`fci` fue desarrollado en **R 4.3**, pero al menos en [Posit (RStudio)
Cloud](https://posit.cloud/) corre en versiones desde **R 3.6.3** en
adelante. Los paquetes `remotes` y `tidyverse` son opcionales, pero el
primero hace la instalación de `fci` más sencilla, y el segundo nos
permite correr los ejemplos en este README:

``` r
# Controlar si `remotes` está cargado, instalar si es necesario
if (!require(remotes)) {
  install.packages("remotes")
}
if (!require(tidyverse)) {
  install.packages("tidyverse")
}

# Usar `remotes` para instalar `fci` directamente desde GitLab
remotes::install_gitlab("gaspanic/fci")
```

# 3 Lista de funciones

<div class="kable-table">

|     | Función                   | Descripción                                            |
|-----|:--------------------------|:-------------------------------------------------------|
| 3   | base_monetaria()          | Datos históricos de la base monetaria de la Argentina  |
| 6   | cartera_fondo()           | Carteras de fondos                                     |
| 8   | cotizacion_dolar()        | Cotizaciones históricas del dólar                      |
| 9   | cotizacion_merval()       | Cotizaciones históricas del índice MERVAL              |
| 10  | dia_laboral()             | ¿Fecha es día laboral?                                 |
| 13  | eventos_historicos()      | Eventos históricos en la Argentina                     |
| 16  | lista_administradoras()   | Administradoras de fondos                              |
| 17  | lista_depositarias()      | Depositarias de fondos                                 |
| 19  | lista_fondos()            | Fondos comunes de inversión                            |
| 22  | rendimiento_fondo()       | Rendimientos históricos de fondos comunes de inversión |
| 24  | tasa_inflacion()          | Tasas históricas de inflación                          |
| 25  | token_estadisticas_bcra() | Obtener token para API de Estadísticas BCRA            |

</div>

Se puede acceder la documentación detallada de cada función usando `?` o
`help()` (p. ej. `?lista_fondos` o `help("lista_fondos")`).

# 4 Ejemplos de uso

``` r
library(tidyverse)
library(fci)
```

## 4.1 Buscar fondos

Usá la función `lista_fondos()` para traer un data frame con todos los
FCI activos en CAFCI:

``` r
# Traer lista de fondos
df_fondos <- lista_fondos()

head(df_fondos)
```

<div class="kable-table">

| nombre                         | clase | id_fondo | id_clase | fuente | moneda | administradora                                   | tipo_renta | region    | benchmark |
|:-------------------------------|:------|:---------|:---------|:-------|:-------|:-------------------------------------------------|:-----------|:----------|:----------|
| Alianza de Capitales           | \-    | 1        | 1        | CAFCI  | ARS    | Administradora de Titulos Valores S.G.F.C.I.S.A. | Renta Fija | Argentina | Badlar    |
| Alpha Planeamiento Conservador | B     | 5        | 5        | CAFCI  | ARS    | ICBC Investments Argentina S.A.U.S.G.F.C.I.      | Renta Fija | Argentina | Otro      |
| Alpha Planeamiento Conservador | A     | 5        | 765      | CAFCI  | ARS    | ICBC Investments Argentina S.A.U.S.G.F.C.I.      | Renta Fija | Argentina | Otro      |
| Alpha Planeamiento Conservador | C     | 5        | 972      | CAFCI  | ARS    | ICBC Investments Argentina S.A.U.S.G.F.C.I.      | Renta Fija | Argentina | Otro      |
| Alpha Planeamiento Conservador | D     | 5        | 973      | CAFCI  | ARS    | ICBC Investments Argentina S.A.U.S.G.F.C.I.      | Renta Fija | Argentina | Otro      |
| Alpha Planeamiento Conservador | I     | 5        | 1834     | CAFCI  | ARS    | ICBC Investments Argentina S.A.U.S.G.F.C.I.      | Renta Fija | Argentina | Otro      |

</div>

Luego, usá tus funciones favoritas para buscar fondos de interés en el
data frame `df_fondos`. Si te interesan fondos de alguna administradora
en particular, usá la función `lista_administradoras()` para obtener un
data frame de todas las administradoras disponibles.

En este ejemplo seleccionamos todos los fondos de la administradora
*Galileo Argentina* que cotizan en pesos argentinos (ARS), y los
comparamos con el fondo de *Mercado Pago*. Además filtramos cada fondo
por su “primera” clase, que generalmente es la clase disponible al
público:

``` r
df_seleccionados <- df_fondos %>%
  # Filtrar fondos por administradora y nombre
  filter(
    administradora == "Galileo Argentina S.G.F.C.I.S.A." |
      nombre == "Mercado Fondo"
  ) %>%
  # Seleccionar solo fondos en pesos argentinos
  filter(moneda == "ARS") %>%
  # Seleccionar la "primera" clase de cada fondo
  group_by(id_fondo) %>%
  filter(id_clase == min(id_clase)) %>%
  ungroup()

df_seleccionados
```

<div class="kable-table">

| nombre                    | clase | id_fondo | id_clase | fuente | moneda | administradora                             | tipo_renta        | region    | benchmark     |
|:--------------------------|:------|:---------|:---------|:-------|:-------|:-------------------------------------------|:------------------|:----------|:--------------|
| Galileo Argentina         | B     | 446      | 2263     | CAFCI  | ARS    | Galileo Argentina S.G.F.C.I.S.A.           | Retorno Total     | Argentina | No Registrado |
| Galileo Ahorro            | B     | 489      | 2260     | CAFCI  | ARS    | Galileo Argentina S.G.F.C.I.S.A.           | Renta Fija        | Argentina | Badlar        |
| Galileo FCI Abierto Pymes | A     | 490      | 968      | CAFCI  | ARS    | Galileo Argentina S.G.F.C.I.S.A.           | PyMes             | Argentina | No Registrado |
| Galileo Premium           | A     | 543      | 1101     | CAFCI  | ARS    | Galileo Argentina S.G.F.C.I.S.A.           | Renta Fija        | Argentina | No Registrado |
| Galileo Acciones          | A     | 615      | 1243     | CAFCI  | ARS    | Galileo Argentina S.G.F.C.I.S.A.           | Renta Variable    | Argentina | S&P Mar       |
| Galileo Ahorro Plus       | A     | 616      | 1244     | CAFCI  | ARS    | Galileo Argentina S.G.F.C.I.S.A.           | Renta Fija        | Argentina | No Registrado |
| Galileo Renta Fija        | A     | 617      | 1245     | CAFCI  | ARS    | Galileo Argentina S.G.F.C.I.S.A.           | Retorno Total     | Argentina | No Registrado |
| Galileo Estrategia        | A     | 748      | 1640     | CAFCI  | ARS    | Galileo Argentina S.G.F.C.I.S.A.           | Renta Fija        | Argentina | No Registrado |
| Mercado Fondo             | A     | 798      | 1982     | CAFCI  | ARS    | Industrial Asset Management S.G.F.C.I.S.A. | Mercado de Dinero | Argentina | Badlar        |
| Galileo Renta             | A     | 840      | 2394     | CAFCI  | ARS    | Galileo Argentina S.G.F.C.I.S.A.           | Renta Fija        | Argentina | No Registrado |
| Galileo Pesos             | A     | 841      | 2398     | CAFCI  | ARS    | Galileo Argentina S.G.F.C.I.S.A.           | Mercado de Dinero | Argentina | No Registrado |
| SF Value                  | A     | 860      | 2481     | CAFCI  | ARS    | Galileo Argentina S.G.F.C.I.S.A.           | Renta Mixta       | Argentina | Otro          |
| Galileo Multimercado II   | A     | 861      | 2482     | CAFCI  | ARS    | Galileo Argentina S.G.F.C.I.S.A.           | Renta Fija        | Argentina | No Registrado |
| Galileo Multimercado III  | A     | 1245     | 3455     | CAFCI  | ARS    | Galileo Argentina S.G.F.C.I.S.A.           | Renta Fija        | Argentina | No Registrado |
| Galileo Multimercado IV   | A     | 1272     | 3543     | CAFCI  | ARS    | Galileo Argentina S.G.F.C.I.S.A.           | Renta Mixta       | Argentina | No Registrado |
| Galileo Sustentable ASG   | A     | 1304     | 3704     | CAFCI  | ARS    | Galileo Argentina S.G.F.C.I.S.A.           | ASG               | Argentina | Otro          |
| Galileo Multimercado V    | A     | 1383     | 3975     | CAFCI  | ARS    | Galileo Argentina S.G.F.C.I.S.A.           | Renta Mixta       | Argentina | No Registrado |
| Galileo Multimercado VI   | A     | 1384     | 3976     | CAFCI  | ARS    | Galileo Argentina S.G.F.C.I.S.A.           | Renta Mixta       | Argentina | No Registrado |

</div>

## 4.2 Datos de rendimiento

Una vez generado un data frame con los fondos de interés, usalo con la
función `rendimiento_fondo()` para traer datos de rendimiento para cada
fondo. La función se puede usar de diferentes maneras:

<font size="2"> ***Importante:** Por más que la entrada a
`rendimiento_fondo()` sea un único data frame, cada fondo implica un
pedido separado a la API. Por ende, cuantos más fondos, más va a tardar
el pedido.*</font>

### 4.2.1 Períodos preestablecidos

Al usar `rendimiento_fondo()` sin definir fechas, la función trae datos
de rendimiento para diferentes períodos preestablecidos, y agrega las
columnas `periodo`, `desde`, `hasta`, `valor_inicio`, `valor_fin`,
`variacion` (en %), `tna` (Tasa nominal anual, en %) y `tca` (Tasa de
crecimiento anual, en %) al data frame de entrada, con cada período
representado por una fila:

``` r
# Obtener rendimientos para fondos elegidos
df_rendimientos <- rendimiento_fondo(df_seleccionados)

# Mostrar resultados
df_rendimientos %>%
  select(-c(id_fondo, id_clase, moneda, administradora, region, benchmark)) %>%
  head()
```

<div class="kable-table">

| nombre                   | clase | fuente | tipo_renta | periodo     | desde      | hasta      | valor_inicio | valor_fin | variacion |      tna |       tca |
|:-------------------------|:------|:-------|:-----------|:------------|:-----------|:-----------|-------------:|----------:|----------:|---------:|----------:|
| Galileo Multimercado III | A     | CAFCI  | Renta Fija | último día  | 2024-02-01 | 2024-02-02 |     6227.467 |  6255.260 |    0.4463 | 163.0111 |  408.5961 |
| Galileo Multimercado III | A     | CAFCI  | Renta Fija | mes actual  | 2024-01-31 | 2024-02-02 |     6136.789 |  6255.260 |    1.9305 | 352.5576 | 3185.0821 |
| Galileo Multimercado III | A     | CAFCI  | Renta Fija | 1 año       | 2023-02-02 | 2024-02-02 |     1906.136 |  6255.260 |  228.1644 | 228.3207 |  228.4316 |
| Galileo Multimercado III | A     | CAFCI  | Renta Fija | 2024        | 2023-12-29 | 2024-02-02 |     5208.741 |  6255.260 |   20.0916 | 209.6702 |  575.7267 |
| Galileo Multimercado III | A     | CAFCI  | Renta Fija | 2023        | 2022-12-30 | 2023-12-29 |     1756.586 |  5208.741 |  196.5264 | 197.2013 |  197.6353 |
| Galileo Multimercado III | A     | CAFCI  | Renta Fija | 2023 a 2024 | 2022-12-30 | 2024-02-02 |     1756.586 |  6255.260 |  256.1033 | 234.4404 |  219.8307 |

</div>

Si listamos todos los períodos devueltos, vemos que representan
diferentes rangos de fechas en los últimos 5 años:

``` r
df_rendimientos %>%
  select(periodo, desde, hasta) %>%
  distinct()
```

<div class="kable-table">

| periodo     | desde      | hasta      |
|:------------|:-----------|:-----------|
| último día  | 2024-02-01 | 2024-02-02 |
| mes actual  | 2024-01-31 | 2024-02-02 |
| 1 año       | 2023-02-02 | 2024-02-02 |
| 2024        | 2023-12-29 | 2024-02-02 |
| 2023        | 2022-12-30 | 2023-12-29 |
| 2023 a 2024 | 2022-12-30 | 2024-02-02 |
| 2022        | 2021-12-30 | 2022-12-30 |
| 2022 a 2024 | 2021-12-30 | 2024-02-02 |
| 2022 a 2023 | 2021-12-30 | 2023-12-29 |
| 3 años      | 2021-02-02 | 2024-02-02 |
| 5 años      | 2019-02-01 | 2024-02-02 |
| 2021        | 2020-12-30 | 2021-12-30 |
| 2020        | 2019-12-30 | 2020-12-30 |
| 2021 a 2024 | 2020-12-30 | 2024-02-02 |
| 2020 a 2024 | 2019-12-30 | 2024-02-02 |
| 2021 a 2023 | 2020-12-30 | 2023-12-29 |
| 2020 a 2023 | 2019-12-30 | 2023-12-29 |
| 2021 a 2022 | 2020-12-30 | 2022-12-30 |
| 2020 a 2022 | 2019-12-30 | 2022-12-30 |
| 2020 a 2021 | 2019-12-30 | 2021-12-30 |

</div>

### 4.2.2 Períodos definidos

Se puede usar la función `rendimiento_fondo()` para traer datos de
rendimiento para períodos específicos, definiendo el período de interés
con los argumentos `desde` y `hasta`. Al solo indicar la fecha `desde`,
la función trae datos hasta la última fecha disponible:

``` r
# Asignar `desde` la fecha tres meses antes de la fecha de hoy
desde <- Sys.Date() - days(90)

# Obtener rendimientos para fondos elegidos
df_rendimientos_def <- rendimiento_fondo(df_seleccionados, desde = desde)

# Mostrar resultados
df_rendimientos_def %>%
  select(-c(id_fondo, id_clase, moneda, administradora, region, benchmark)) %>%
  head(10)
```

<div class="kable-table">

| nombre                   | clase | fuente | tipo_renta     | periodo                 | desde      | hasta      | valor_inicio |  valor_fin | variacion |      tna |       tca |
|:-------------------------|:------|:-------|:---------------|:------------------------|:-----------|:-----------|-------------:|-----------:|----------:|---------:|----------:|
| Galileo Multimercado III | A     | CAFCI  | Renta Fija     | 2023-11-07 a 2024-02-02 | 2023-11-07 | 2024-02-02 |     3849.429 |   6255.260 |  62.49839 | 262.3855 |  667.7193 |
| Galileo Multimercado IV  | A     | CAFCI  | Renta Mixta    | 2023-11-07 a 2024-02-02 | 2023-11-07 | 2024-02-02 |     3563.828 |   6324.803 |  77.47217 | 325.2496 | 1011.5208 |
| Galileo Sustentable ASG  | A     | CAFCI  | ASG            | 2023-11-07 a 2024-02-02 | 2023-11-07 | 2024-02-02 |     3238.941 |   4861.911 |  50.10804 | 210.3674 |  450.2911 |
| Galileo Multimercado V   | A     | CAFCI  | Renta Mixta    | 2023-11-07 a 2024-02-02 | 2023-11-07 | 2024-02-02 |     1816.881 |   3476.171 |  91.32629 | 383.4130 | 1423.9394 |
| Galileo Multimercado VI  | A     | CAFCI  | Renta Mixta    | 2023-11-07 a 2024-02-02 | 2023-11-07 | 2024-02-02 |     1000.000 |   1000.000 |   0.00000 |   0.0000 |    0.0000 |
| Galileo Argentina        | B     | CAFCI  | Retorno Total  | 2023-11-07 a 2024-02-02 | 2023-11-07 | 2024-02-02 |   143541.335 | 232739.545 |  62.14113 | 260.8856 |  660.6582 |
| Galileo Ahorro           | B     | CAFCI  | Renta Fija     | 2023-11-07 a 2024-02-02 | 2023-11-07 | 2024-02-02 |    44773.170 |  57108.938 |  27.55170 | 115.6696 |  177.7794 |
| Galileo Premium          | A     | CAFCI  | Renta Fija     | 2023-11-07 a 2024-02-02 | 2023-11-07 | 2024-02-02 |    99888.172 | 158057.897 |  58.23485 | 244.4860 |  586.6341 |
| Galileo Acciones         | A     | CAFCI  | Renta Variable | 2023-11-07 a 2024-02-02 | 2023-11-07 | 2024-02-02 |    88662.781 | 182790.515 | 106.16375 | 445.7047 | 1985.2107 |
| Galileo Ahorro Plus      | A     | CAFCI  | Renta Fija     | 2023-11-07 a 2024-02-02 | 2023-11-07 | 2024-02-02 |    24353.256 |  31074.820 |  27.60027 | 115.8735 |  178.2237 |

</div>

### 4.2.3 Movimientos

Si además de definir fechas, pasás el argumento `movimientos = TRUE` a
`rendimiento_fondo()`, la función trae datos de variación entre fechas
intermedias (movimientos) para el período establecido. Esto trae las
columnas `fecha`, `valor` (valor de cuotapartes), `valor_rel` (valor
relativizado/normalizado a la fecha inicial) y `variacion`, con cada
fecha representada por una fila:

``` r
# Obtener rendimientos para fondos elegidos
df_rendimientos_movimientos <- rendimiento_fondo(df_seleccionados,
                                                 desde = desde,
                                                 movimientos = TRUE)

# Mostrar primeros resultados
df_rendimientos_movimientos %>%
  select(-c(id_fondo, id_clase, moneda, administradora, region, benchmark)) %>%
  head(10)
```

<div class="kable-table">

| nombre                   | clase | fuente | tipo_renta | fecha      |    valor | valor_rel |  variacion |
|:-------------------------|:------|:-------|:-----------|:-----------|---------:|----------:|-----------:|
| Galileo Multimercado III | A     | CAFCI  | Renta Fija | 2023-11-07 | 3849.429 |  1.000000 |         NA |
| Galileo Multimercado III | A     | CAFCI  | Renta Fija | 2023-11-15 | 3962.588 |  1.029396 |  2.9396308 |
| Galileo Multimercado III | A     | CAFCI  | Renta Fija | 2023-11-23 | 4746.593 |  1.233064 | 19.7851757 |
| Galileo Multimercado III | A     | CAFCI  | Renta Fija | 2023-11-27 | 4504.924 |  1.170284 | -5.0914203 |
| Galileo Multimercado III | A     | CAFCI  | Renta Fija | 2023-12-01 | 4682.487 |  1.216411 |  3.9415315 |
| Galileo Multimercado III | A     | CAFCI  | Renta Fija | 2023-12-05 | 4743.227 |  1.232190 |  1.2971739 |
| Galileo Multimercado III | A     | CAFCI  | Renta Fija | 2023-12-13 | 5216.898 |  1.355239 |  9.9862604 |
| Galileo Multimercado III | A     | CAFCI  | Renta Fija | 2023-12-21 | 5097.497 |  1.324222 | -2.2887356 |
| Galileo Multimercado III | A     | CAFCI  | Renta Fija | 2023-12-29 | 5208.740 |  1.353120 |  2.1823063 |
| Galileo Multimercado III | A     | CAFCI  | Renta Fija | 2024-01-02 | 5243.865 |  1.362245 |  0.6743473 |

</div>

## 4.3 Graficar rendimientos

Para graficar rendimientos de fondos, usá las funciones de `ggplot2`.

### 4.3.1 Rendimientos totales

Aquí usamos el data frame de [períodos
preestablecidos](#421-períodos-preestablecidos) generado anteriormente,
para primero elegir algunos períodos de interés, y luego graficar el
rendimiento de cada fondo en un gráfico de barras. La *Tasa de
crecimiento anual* (TCA) usada acá representa el rendimiento anualizado,
lo que permite comparar rendimientos de períodos de largos diferentes:

``` r
periodos <- c("2020", "2021", "2022", "2023", "2024", "2020 a 2024")

df_rendimientos %>%
  # Filtrar por períodos preestablecidos de interés
  filter(periodo %in% !!periodos) %>%
  # Generar gráfico de barras
  ggplot(aes(x = tca, y = nombre, fill = tipo_renta)) +
  geom_col() +
  scale_y_discrete(limits = rev) +
  labs(x = "Tasa de crecimiento anual (%)", y = NULL) +
  # Ordenar facetas por período
  facet_wrap(. ~ factor(periodo, levels = periodos), scales = "free_x") +
  theme_minimal()
```

![](README_files/figure-gfm/plot_yields-1.png)<!-- -->

### 4.3.2 Evolución de rendimientos (movimientos)

Para comparar [movimientos](#423-movimientos) entre fondos conviene usar
la columna `valor_rel`, que representa un valor relativizado/normalizado
de las cuotapartes:

``` r
df_rendimientos_movimientos %>%
  ggplot(aes(x = fecha, y = valor_rel, color = nombre, group = nombre)) +
  geom_line() +
  scale_y_continuous(trans = "log10") +
  theme_minimal()
```

![](README_files/figure-gfm/plot_movements-1.png)<!-- -->

## 4.4 Otras métricas: dólar, inflación y MERVAL

Además de traer datos de fondos, `fci` viene también con funciones para
acceder datos de otras métricas relevantes para evaluar si los FCI
representan una buena forma de inversión, o no:

- `cotizacion_dolar()` trae datos históricos para diferentes cambios del
  dólar
- `tasa_inflacion()` trae datos históricos de la inflación mensual
  (según BCRA)
- `cotizacion_merval()` trae datos históricos del índice MERVAL

Los data frames generados por estas funciones son compatibles con los
generados por `rendimiento_fondo()`, lo que hace muy fácil la
combinación y visualización de los datos en forma conjunta.

En este ejemplo obtenemos datos del dólar oficial, informal (blue), la
inflación mensual y el índice MERVAL sin definir fechas. Esto trae datos
históricos para los mismos períodos preestablecidos como los generados
por `rendimiento_fondo()`:

``` r
# Definir correo electrónico del usuario
correo_electronico <- "usuario@correo.com"
```

``` r
# Obtener token para poder utilizar la API de Estadísticas BCRA
bcra_token <- token_estadisticas_bcra(correo_electronico)

df_dolar <- cotizacion_dolar("oficial", "blue")
df_inflacion <- tasa_inflacion(bcra_token)
df_merval <- cotizacion_merval(bcra_token)

df_combinados <- bind_rows(df_rendimientos,
                           df_dolar,
                           df_inflacion,
                           df_merval)

periodos <- c("2020", "2021", "2022", "2023", "2024", "2020 a 2024")

df_combinados %>%
  # Filtrar por períodos preestablecidos de interés
  filter(periodo %in% !!periodos) %>%
  # Generar gráfico de barras
  ggplot(aes(x = tca, y = nombre, fill = tipo_renta)) +
  geom_col() +
  scale_y_discrete(limits = rev) +
  labs(x = "Tasa de crecimiento anual (%)", y = NULL) +
  # Ordenar facetas por período
  facet_wrap(. ~ factor(periodo, levels = periodos), scales = "free_x") +
  theme_minimal()
```

![](README_files/figure-gfm/metrics_default_periods-1.png)<!-- -->

## 4.5 Comparar categorías de fondos

Como último ejemplo, compararemos *categorías* de fondos (`tipo_renta`).
Primero hacemos un muestreo de hasta 10 fondos de cada categoría de
*todas* las administradoras disponibles. Luego pedimos rendimientos
históricos para estos fondos, y finalmente graficamos las distribuciones
de rendimientos de cada categoría con un gráfico de cajas:

``` r
df_fondos_muestreo <- df_fondos %>%
  # Seleccionar solo fondos en pesos argentinos
  filter(moneda == "ARS") %>%
  # Seleccionar la "primera" clase de cada fondo
  group_by(id_fondo) %>%
  filter(clase == min(clase)) %>%
  filter(id_clase == min(id_clase)) %>%
  ungroup() %>%
  # Tomar muestra de <=10 fondos de cada categoría `tipo_renta`
  group_by(tipo_renta) %>%
  slice_sample(n = 10) %>%
  ungroup()

# Traer rendimientos para muestreo de fondos
df_rendimientos_muestreo <- df_fondos_muestreo %>%
  rendimiento_fondo()

# Definir períodos de interés
periodos <- c("1 año", "3 años", "5 años")

# Graficar rendimientos
df_rendimientos_muestreo %>%
  filter(periodo %in% !!periodos) %>%
  ggplot(aes(y = tipo_renta, x = tca, fill = tipo_renta)) +
  geom_boxplot(outlier.size = 1, na.rm = TRUE) +
  theme_minimal() +
  labs(x = "Tasa de crecimiento anual (%)", y = NULL) +
  facet_wrap(. ~ periodo, scales = "free_x")
```

![](README_files/figure-gfm/sampled_funds-1.png)<!-- -->

# 5 Lista de endpoints conocidos de la API de CAFCI

| Descripción                               | URL de ejemplo                                                                           | Comentario                                                                                                                                                             |
|:------------------------------------------|:-----------------------------------------------------------------------------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Lista de fondos                           | <https://api.cafci.org.ar/fondo?limit=0>                                                 | El argumento `?limit=0` inactiva la paginación                                                                                                                         |
| Fondo específico                          | <https://api.cafci.org.ar/fondo/615>                                                     |                                                                                                                                                                        |
| Clases de fondo específico                | <https://api.cafci.org.ar/fondo/615/clase/>                                              |                                                                                                                                                                        |
| Clase y fondo específicos                 | <https://api.cafci.org.ar/fondo/615/clase/1243>                                          |                                                                                                                                                                        |
| Detalles de clase y fondo específicos     | <https://api.cafci.org.ar/fondo/615/clase/1243/ficha>                                    |                                                                                                                                                                        |
| Filtrar lista de fondos por ID            | <https://api.cafci.org.ar/fondo?id=615>                                                  | Filtrar por multiples fondos con `?id=615&id=616&id=617...`                                                                                                            |
| Lista de fondos con clases                | <https://api.cafci.org.ar/fondo?include=clase_fondo>                                     | Otros parámetros: `benchmark`, `clase_fondo`, `duration`, `entidad;depositaria`, `entidad;gerente`, `horizonte`, `region`, `tipo_fondo`, `tipoRenta`, `tipoRentaMixta` |
| Filtrar lista de fondos por fondo y clase | <https://api.cafci.org.ar/fondo?id=615&include=clase_fondo&clase_fondo.id=1243>          |                                                                                                                                                                        |
| Lista de fondos con clases ordenada       | <https://api.cafci.org.ar/fondo?limit=0&include=clase_fondo&order=clase_fondos.nombre>   |                                                                                                                                                                        |
| Rendimiento total entre fechas            | <https://api.cafci.org.ar/fondo/615/clase/1243/rendimiento/2021-01-04/2023-05-05?step=0> |                                                                                                                                                                        |
| Movimientos entre fechas                  | <https://api.cafci.org.ar/fondo/615/clase/1243/rendimiento/2021-01-04/2023-05-05?step=1> |                                                                                                                                                                        |
| Lista de feriados                         | <https://api.cafci.org.ar/feriado?limit=0>                                               |                                                                                                                                                                        |
| Lista de categorías de fondos             | <https://api.cafci.org.ar/tipo-renta>                                                    |                                                                                                                                                                        |
| Información diaria de categoría de fondos | <https://api.cafci.org.ar/estadisticas/informacion/diaria/2/2023-05-17>                  |                                                                                                                                                                        |
| Lista de monedas                          | <https://api.cafci.org.ar/moneda?limit=0>                                                |                                                                                                                                                                        |
| Lista de tipos de clases                  | <https://api.cafci.org.ar/fondo/tipo-clase>                                              |                                                                                                                                                                        |
| Lista de entidades                        | <https://api.cafci.org.ar/entidad?limit=0>                                               |                                                                                                                                                                        |
| Métodos de contacto entidades             | <https://api.cafci.org.ar/entidad/1/comunicacion?limit=0>                                |                                                                                                                                                                        |
| Número de asociadas por año               | <https://api.cafci.org.ar/estadisticas/cantidad/asociadas>                               |                                                                                                                                                                        |
| Resumen patrimonial                       | <https://api.cafci.org.ar/estadisticas/resumen/patrimonial/2023-05-18>                   |                                                                                                                                                                        |
